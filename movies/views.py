from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
import redis
import json
import requests

# Connect to our Redis instance
redis_instance = redis.StrictRedis(host=settings.REDIS_HOST,
                                  port=settings.REDIS_PORT, db=0)

def index(request, title='', actor=''):
    data = []
    try:
        data = json.loads(redis_instance.get('data'))
    except:
        data = get_data()
    
    res = []
    for key in data:
        el = data[key]
        if (title.lower() in el['movie']['title'].lower()) and (actor == '' or (actor.lower() in [actor['name'].lower() for actor in el['actors']])):
            res.append(el)

    return HttpResponse(json.dumps(res))


def get_data():
    res = {}
    try:
        movies = requests.get('https://ghibliapi.herokuapp.com/films/').json()
        actors = requests.get('https://ghibliapi.herokuapp.com/people/').json()
        for actor in actors:
            actorMovies = filter(lambda movie: movie['url'] in actor['films'], movies)
            for movie in actorMovies:
                id = movie['id']
                if id in res:
                    res[id]['actors'].append(actor)
                else:
                    res[id] = {'movie': movie, 'actors': [actor]}

        res = json.dumps(res)
    except:
        res['message'] = 'Experiencing problems with third party API'
    
    return res
