from django.urls import include, path
from . import views

urlpatterns = [
    path('/', views.index),
    path('/title/<title>/', views.index),
    path('/actor/<actor>/', views.index),
    path('/title/<title>/actor/<actor>/', views.index),
]
