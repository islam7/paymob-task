from celery import shared_task
import requests
import json
from django.conf import settings
import redis
from . import views


# Connect to our Redis instance
redis_instance = redis.StrictRedis(host=settings.REDIS_HOST,
                                  port=settings.REDIS_PORT, db=0)

@shared_task
def update_cache():
    res = views.get_data()
    redis_instance.set('data', res)