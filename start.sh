virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
python manage.py migrate
celery -A paymob_task worker --beat --scheduler django --loglevel=info --detach
python manage.py runserver

