import os
from celery import Celery
from django.conf import settings
from datetime import timedelta


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "paymob_task.settings")

app = Celery('paymob_task', )

app.config_from_object('django.conf:settings', namespace="CELERY")

app.autodiscover_tasks()

@app.task(bind=True)
def debug_task(self):
    print(f'Request: {self.request!r}')